//Get API keys in order to connect to Andromeda
var CSI = new CSInterface();

var _SYSTEM	= null;
var _COMPANY= null;
var _USER	= null;
var _APIKEY	= null;
var _APIURL	= null;
var _ATOKEN	= "";
var _LOCALGALLERY = CSI.getSystemPath(SystemPath.MY_DOCUMENTS) + "/NGC/Library/";

setAndromedaKeys();

//FUNCTIONS
function setAndromedaKeys()
{
	document.getElementById("btnpreparedata").style.display="none";
	var pathDataStorage = CSI.getSystemPath(SystemPath.USER_DATA);
	CSI.evalScript("getAndromedaInfo('"+pathDataStorage+"');", readSettings);
}

function readSettings(event)
{
	var settings = JSON.parse(event);
	_SYSTEM	= settings.url;
	_COMPANY= settings.company;
	_USER	= settings.username.toUpperCase();
	_APIKEY	= settings.sessionid;
	_APIURL	= _SYSTEM + "api/v1/";
	//document.getElementById("lblUser").innerHTML= "Hello " + _USER;
	getValidTokenForThumbnails();
}