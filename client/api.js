var ISDEBUG = false;
var _COMPLETE_LOAD_ON	= 5; //To match number of entities to load.
var _COMPLETE_DONE		= 0;

function APICALL(mode,apiresource,method,_entityPath,_code)
{
	//----------------------------------- DEBUG -----------------------------------
	//Uncomment code below to run the web-page isolated of AI
	//OF COURSE you need to use your own values.
	if (ISDEBUG)
	{
		_SYSTEM	="http://localhost/SALES/";
		_USER	="SOMESH";
		_APIKEY	="eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiIyIiwiaXNzIjoibmdjYXBwOkRBTSIsImRldiI6Im5nY2RldmVsb3BlciIsInNjb3BlIjoiU0FMRVMiLCJqdGkiOiIzMmRhNGJiZS1kNTIzLTQ4NGEtYmQ2Yy03M2ZlYjZhM2Y1ZDMiLCJhdWQiOlsiUEFDS0FHRSIsIlNBTEVTIiwiTW9EQURlc2t0b3AvMTUuMiAoV2luMzJOVCkiLCI6OjE6OjoxIl0sImlhdCI6MTU2Mzk4MTU1OCwiZXhwIjoxNTcxNzU3NTU4fQ.vs1H7FU1-BF42YyfvEBSjRAv8xfsC2vh63WScBQZzKw";
		_APIURL	= _SYSTEM + "api/v1/";
		_LOCALGALLERY = "C:/Users/amartel/Documents/NGC/Library/";
	}
	//----------------------------------- DEBUG -----------------------------------

	var HTTPContType = "",HTTPRespType = "";
	var entity = "",fileid="", code = "";
	switch (mode) {
		case "file":
			HTTPContType = "application/octet-stream";
			HTTPRespType = "arraybuffer";
			entity = _entityPath;
			fileid = apiresource.substr(apiresource.indexOf('id/')+3);
			fileid = fileid.substr(0,fileid.indexOf('/'));
			code	= _code;
			break;
		default:
			HTTPContType = "application/json";
			HTTPRespType = "json";
			entity = apiresource.substr(apiresource.lastIndexOf('/')+1);
			break;
	}
	
	var _APIResource,_Method;
	if (window.XMLHttpRequest)
		var http_request = new XMLHttpRequest();
	else
		if (window.ActiveXObject)
			var http_request = new ActiveXObject("Microsoft.XMLHTTP");

	if (http_request) {
		_APIResource = apiresource;
		_Method = method;

		http_request.onreadystatechange = function () { eval("APICALLBACK(http_request,HTTPRespType,entity,fileid,code)"); };
		http_request.open(_Method, _APIURL+_APIResource, true);
		http_request.setRequestHeader("Authorization","Bearer "+_APIKEY);
		http_request.setRequestHeader("Content-Type",HTTPContType);
		http_request.responseType = HTTPRespType;
		http_request.send();
	}
}

function APICALLBACK(http_request,respType,entity,fileid,code)
{
	if(http_request.readyState==4)
	{
		if(http_request.status==200)
		{
			switch (respType) {
				case "arraybuffer":
						saveData(http_request.response,entity,fileid,code);
					break;
				case "json":
						parseData(http_request.response,entity);
						break;
				default:
					break;
			}
		}
		else
		{
			APICALLOnError(http_request);
		}
	}
}

function parseData(rawData,entity)
{
	if (ISDEBUG){ var _FOLDERENTITY = _LOCALGALLERY + entity + "/";	}
	else
	{
		//NodeJS
		var fs = require('fs');
		var _FOLDERENTITY = _LOCALGALLERY + entity + "/";
		
		if(!fs.existsSync(_LOCALGALLERY))
		{
			fs.mkdirSync(_LOCALGALLERY);
		}
		
		if(!fs.existsSync(_FOLDERENTITY))
		{
			fs.mkdirSync(_FOLDERENTITY);
		}
	}
	
	var dataTable = [], dataRow = [];
	var _BO = rawData;
	var _iItem = 0;
	for(_iItem=0;_iItem<_BO.Records.length;_iItem++)
	{
		var _IMGID = _BO.Records[_iItem].ImgID;
		_IMGID = _IMGID === null ? -1 : _IMGID;

		var _IMGVersion = _BO.Records[_iItem].ImgVer;

		var DAThumbURL = _SYSTEM + "api/daimages/" + _IMGID + "/SM/" + _IMGVersion + "/0/" + _COMPANY + _ATOKEN;
		dataRow = [ DAThumbURL, 
					_IMGID === -1 ? "" : _IMGID, _BO.Records[_iItem].RV,
					_BO.Records[_iItem].SD.substring(_BO.Records[_iItem].RV.toString().length+3),
					_IMGID];
		dataTable.push(dataRow);
	}
	var dt = $('#dt'+entity).DataTable();
	dt.rows.add(dataTable).draw(false);
	notifyAllLoaded();
}

function notifyAllLoaded()
{
	_COMPLETE_DONE += 1;
	if(_COMPLETE_DONE == _COMPLETE_LOAD_ON)
	{
		alertify.success('Ready');
		setTimeout(function(){ alertify.dismissAll(); }, 5000);
	}
}

function saveData(rawData,entity,fileid,code)
{
	if (window.XMLHttpRequest)
		var http_request = new XMLHttpRequest();
	else
		if (window.ActiveXObject)
			var http_request = new ActiveXObject("Microsoft.XMLHTTP");

	if (http_request) {
		http_request.onreadystatechange = function () { eval("saveDataWithImageName(http_request,rawData,entity,fileid,code)"); };
		http_request.open("GET", _APIURL + "bo/digitalasset/"+fileid, true);
		http_request.setRequestHeader("Authorization","Bearer "+_APIKEY);
		http_request.setRequestHeader("Content-Type","application/json");
		http_request.responseType = "json";
		http_request.send();
	}
}

function saveDataWithImageName(http_request,rawData,entity,fileid,code)
{
	if(http_request.readyState==4)
	{
		if(http_request.status==200)
		{
			var _Entity = http_request.response.Entity;
			var _DAFileName = _Entity.name;
			var _DAFileVersion = _Entity.fileversion;
			var _DAFileExt	= _DAFileName.substr(_DAFileName.lastIndexOf('.'));
			
			//------------------- NodeJS -------------------
			var fs = require('fs');
			var _FOLDERENTITY = _LOCALGALLERY + entity + "/";
			var byteArray = new Uint8Array(rawData);
			fs.writeFileSync(_FOLDERENTITY + fileid + "_" +  _DAFileName, byteArray, function (err) {
				if (err) throw err;
			});
			
			alertify.success("Ext: " + _DAFileExt + " - Version: " + _DAFileVersion);
			preparePlace(code,fileid,entity,true);
		}
		else
		{
			APICALLOnError(http_request);
		}
	}
}

function APICALLOnError(requestobject)
{
	var _eMsg = "";
	switch (requestobject.status) {
		case 0:
			_eMsg = "NO RESPONSE";
			break;
		case 440:
			_eMsg = requestobject.response.Message;
			break;
		default:
			_eMsg = requestobject.statusText;
	}
	alertify.error(_eMsg);
}