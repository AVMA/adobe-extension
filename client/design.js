//#region GLOBALS
var _VALIDENTITIES	= {"Entity": [], "Title" : [], "Fields": [], "IMG" : [], "IsOnlyCopy" : []};
var _MORE4CREATE	= {"Entity": [], "Title" : [], "Fields": [], "IMG" : [], "IsOnlyCopy" : []};
var ISTOKENDONE = false;

var _TEMPLATE = '';
_TEMPLATE += '<button class="collapsible">##title##</button>';
_TEMPLATE += '<div class="content">';
_TEMPLATE += '<table id="dt##entity##" class="table table-hover table-borderless table-sm" cellspacing="0" width="100%">';
_TEMPLATE += '	<thead>';
_TEMPLATE += '		<tr>';
_TEMPLATE += '			<th class="th-sm">Image</th>';
_TEMPLATE += '			<th class="th-sm">Image ID</th>';
_TEMPLATE += '			<th class="th-sm">Code</th>';
_TEMPLATE += '			<th class="th-sm"></th>';
_TEMPLATE += '			<th class="th-sm"></th>';
_TEMPLATE += '		</tr>';
_TEMPLATE += '	</thead>';
_TEMPLATE += '	<tbody>';
_TEMPLATE += '	</tbody>';
_TEMPLATE += '</table>';
_TEMPLATE += '</div>';

var _INPUTTEXT	= '';
_INPUTTEXT += '<div class="md-form form-sm">';
_INPUTTEXT += '	<input type="text" id="f##field##" class="form-control form-control-sm ##class##" value="##.##">';
_INPUTTEXT += '	<label for="f##field##">##label##</label>';
_INPUTTEXT += '</div>';

var _TEXTAREA	= '';
_TEXTAREA += '<div class="md-form form-sm">';
_TEXTAREA += '	<textarea type="text" id="f##field##" class="md-textarea form-control"></textarea>';
_TEXTAREA += '	<label for="f##field##">##label##</label>';
_TEXTAREA += '</div>';

var _DOCUMENT_IMG_ID_FROM_MoDA = -1;
//#endregion

function getDocumentName(doCallback, CallBackData)
{
	_DOCUMENT_IMG_ID_FROM_MoDA = -1
	if(document.getElementById('checkImage'))
		$('#checkImage').removeClass('invisible');

	$("#docName").val('thefile.ai');
	CSI.evalScript("JSXgetDocumentName();", function(data)
	{
		$("#docName").val(data);
	});
	
	var pathDataStorage = CSI.getSystemPath(SystemPath.USER_DATA);
	CSI.evalScript("getDAMDesktop_FileSync('"+pathDataStorage+"');", function(data)
	{
		var _FILEID = -1;
		var MoDA = JSON.parse(data);
		for (let iRecord = 0; iRecord < MoDA.length; iRecord++) {
			var fileName = MoDA[iRecord].Data._FileName;
			fileName = fileName == null ? "" : fileName;
			if(fileName.toLowerCase() == $("#docName").val().toLowerCase())
			{
				_FILEID = MoDA[iRecord].Data._FileID;
				break;
			}
		}
		if(_FILEID == -1)
		{
			_DOCUMENT_IMG_ID_FROM_MoDA = -1;
			if(document.getElementById('checkImage'))
				$('#checkImage').addClass('invisible');
			//alertify.error('Image not available in MoDA DB. Can\'t be assigned to the entity');
		}
		else
		{
			_DOCUMENT_IMG_ID_FROM_MoDA = _FILEID;
		}
		
		if(doCallback)
		{
			updateDigitalAssetRecord(CallBackData)
		}
	});
}

function getPlacedItems()
{
	CSI.evalScript("getPlacedItemsList();", function(data)
	{
		if(data == "")
		{
			alertify.error("No EMBEDDED items found");
		}
		else
		{
			prepareDocumentID(data);
		}
	});
}

function prepareDocumentID(data)
{
	getDocumentName(true,data);
}

function updateDigitalAssetRecord(data)
{
	if(_DOCUMENT_IMG_ID_FROM_MoDA > -1)
	{
		alertify.success("Updating...");
		if (window.XMLHttpRequest)
			var http_request = new XMLHttpRequest();
		else
			if (window.ActiveXObject)
				var http_request = new ActiveXObject("Microsoft.XMLHTTP");

		if (http_request) {
			var childDetail = '"childdetail":"' + data.replace(/\"/g,'\\"') +'"';

			var __dataParams = '{"Entity": { ' + childDetail + ' }}';
			http_request.onreadystatechange = function () { eval("afterUpdateDA(http_request,data)"); };
			http_request.open("POST", _APIURL + "bo/digitalasset/" + _DOCUMENT_IMG_ID_FROM_MoDA, true );
			http_request.setRequestHeader("Authorization","Bearer "+_APIKEY);
			http_request.setRequestHeader("Content-Type","application/json");
			http_request.responseType = "json";
			http_request.send(__dataParams);
		}
	}
	else
	{
		//not sync with MoDA. Just compied to clipboard
		copyToClipboard(data,true);
	}
}

function afterUpdateDA(http_request,data)
{
	if(http_request.readyState==4)
	{
		alertify.dismissAll();
		if(http_request.status==200)
		{
			var IsSuccess = http_request.response.IsSuccess;
			if(IsSuccess){ alertify.success("Updated, pending to Sync with MoDA"); }
			else { alertify.error(http_request.response.Result); }
		}
		else
		{
			//APICALLOnError(http_request);
			//OnError, just copy to clipboard. Manual update
			copyToClipboard(data,true);
		}
	}
}

function getValidTokenForThumbnails()
{
	//Taken value from SM
	alertify.success('Requesting token to display thumbnails...');
	if (window.XMLHttpRequest)
		var http_request = new XMLHttpRequest();
	else
		if (window.ActiveXObject)
			var http_request = new ActiveXObject("Microsoft.XMLHTTP");

	if (http_request) {
		http_request.onreadystatechange = function () { eval("getatoken(http_request)"); };
		http_request.open("GET", _APIURL + "bo/CompanyUser/1", true);
		http_request.setRequestHeader("Authorization","Bearer "+_APIKEY);
		http_request.setRequestHeader("Content-Type","application/json");
		http_request.responseType = "json";
		http_request.send();
	}
}

function getatoken(http_request)
{
	if(http_request.readyState==4)
	{
		ISTOKENDONE = true;
		if(http_request.status==200)
		{
			var _Entity = http_request.response.Entity;
			if(_Entity.token == "")
				alertify.error("Token was not found");
			_ATOKEN = '?atoken=' + _Entity.token;
		}
		else
		{
			APICALLOnError(http_request);
		}

		prepareDataTables();
	}
}

function prepareDataTables()
{
	alertify.success('Loading data...',600);
	
	//Preparing HTML. Data could come from Andromeda. TBD
	loadMETADATA('season','Season','season|seasondescription','n/a',true);
	loadMETADATA('component','Components','component|description','id_digitalasset_mainimage');
	loadMETADATA('color','Colors','color|colordescription','id_digitalasset_mainimage');
	//loadMETADATA('colorlibrary','Color Library','colorsystem|reference|referencename|colorfamily|red|green|blue','id_digitalasset_mainimage');
	loadMETADATA('labelpackcode','Label and Pack Codes','labelpackcode|step|labelpackcodedescription','id_digitalasset_labelpackimage');
	loadMETADATA('constructioncode','Construction Codes','constructioncode|part|step|constructioncodedescription','id_digitalasset_mainimage');

	//More entities (if any) to be created in ANDROMEDA within ADOBE extension.
	loadOtherForCreateEntity('developmentstyle','Development Styles','style|season|styledescription','id_digitalasset_mainimage');
	// -----------------------------------------------------------------------

	var mainHTML = '';
	for (let iValid = 0; iValid < _VALIDENTITIES.Entity.length; iValid++)
	{
		var eTemplate = _TEMPLATE;
		eTemplate = eTemplate.replace('##title##' ,_VALIDENTITIES.Title[iValid]);
		eTemplate = eTemplate.replace('##entity##',_VALIDENTITIES.Entity[iValid]);
		mainHTML += eTemplate;
	}
	makeCollabsible(mainHTML);

	makeSelectEntities(); //_MORE4CREATE will hold all definitions from this point.

	for (let iValid = 0; iValid < _VALIDENTITIES.Entity.length; iValid++)
	{
		doDataTable(_VALIDENTITIES.Title[iValid],_VALIDENTITIES.Entity[iValid]);
	}
}

function loadMETADATA(_entity,_title,_fields,_image,_isCopyClipboard)
{
	_VALIDENTITIES.Entity.push(_entity);
	_VALIDENTITIES.Title.push(_title);
	_VALIDENTITIES.Fields.push(_fields);
	_VALIDENTITIES.IMG.push(_image);
	if(_isCopyClipboard){ _VALIDENTITIES.IsOnlyCopy.push(true); }
	else{ _VALIDENTITIES.IsOnlyCopy.push(false); }
}

function loadOtherForCreateEntity(_entity,_title,_fields,_image)
{
	_MORE4CREATE.Entity.push(_entity);
	_MORE4CREATE.Title.push(_title);
	_MORE4CREATE.Fields.push(_fields);
	_MORE4CREATE.IMG.push(_image);
	_MORE4CREATE.IsOnlyCopy.push(false);
}

//test comment
function makeCollabsible(mainHTML)
{
	$("div.data-all-container").html(mainHTML);
	var collap = document.getElementsByClassName("collapsible");
	var iSection;
	for (iSection = 0; iSection < collap.length; iSection++)
	{
		collap[iSection].addEventListener("click", function ()
		{
			this.classList.toggle("typeactive");
			var content = this.nextElementSibling;
			if (content.style.maxHeight) {
				content.style.maxHeight = null;
			} else {
				content.style.maxHeight = content.scrollHeight + "px";
			}
		});
	}
}

function makeSelectEntities()
{
	_MORE4CREATE.Entity	= _MORE4CREATE.Entity.concat(_VALIDENTITIES.Entity);
	_MORE4CREATE.Title	= _MORE4CREATE.Title.concat(_VALIDENTITIES.Title);
	_MORE4CREATE.Fields	= _MORE4CREATE.Fields.concat(_VALIDENTITIES.Fields);
	_MORE4CREATE.IMG	= _MORE4CREATE.IMG.concat(_VALIDENTITIES.IMG);
	_MORE4CREATE.IsOnlyCopy	= _MORE4CREATE.IsOnlyCopy.concat(_VALIDENTITIES.IsOnlyCopy);
	for (let iOption = 0; iOption < _MORE4CREATE.Entity.length; iOption++)
	{
		var _TheTitle = _MORE4CREATE.Title[iOption];
		_TheTitle = (_TheTitle.substr(-1).toLowerCase() == 's' ? _TheTitle.substr(0,_TheTitle.length-1) : _TheTitle);
		$("select.custom-select").append('<option value="'+_MORE4CREATE.Entity[iOption]+'">'+_TheTitle+'</option>');
	}
	$("select.custom-select").on('change', function() {
		displayValidFields(this.value);
	});
}

function displayValidFields(_entity)
{
	var _aIndex = _MORE4CREATE.Entity.indexOf(_entity);
	var fieldsEntity = _MORE4CREATE.Fields[_aIndex].split('|');
	var fieldsHTML = '';
	var allowImg	= _MORE4CREATE.IMG[_aIndex] != "n/a";

	for (let iField = 0; iField < fieldsEntity.length; iField++)
	{
		var eField = _INPUTTEXT;
		eField	= eField.replace(/\##field##/g, fieldsEntity[iField]);
		eField	= eField.replace(/\##label##/g, '' + fieldsEntity[iField]); //*!* Label for...
		eField	= eField.replace(/\##class##/g, (iField == 0 ? "keyfield4docname" : ""));
		eField	= eField.replace(/\##.##/g, (iField == 0 ? "." : ""));
		fieldsHTML += eField;
	}
	fieldsHTML += '<div class="form-group ' + (_DOCUMENT_IMG_ID_FROM_MoDA > -1 && allowImg? '' : 'invisible') + '" id="checkImage">';
	fieldsHTML += '	<input class="form-check-input" type="checkbox" id="id_digitalasset" ' + (allowImg? "checked" : "") + '>';
	fieldsHTML += '	<label for="id_digitalasset" class="form-check-label">Associate image: '+ $("#docName").val() +'</label>';
	fieldsHTML += '</div>';

	$("div.fields-per-entity").html(fieldsHTML);

	checkKeyEntered = $('input.keyfield4docname').val();
	thisDocName		= $("#docName").val();
	if(checkKeyEntered == '.' || checkKeyEntered == thisDocName)
	{
		$('input.keyfield4docname').val(thisDocName.substring(0, thisDocName.lastIndexOf('.')));
		$('input.keyfield4docname').focus();
	}
}

function doDataTable(_title,_entity)
{
	var _aIndex = _MORE4CREATE.Entity.indexOf(_entity);
	$('#dt'+_entity).DataTable({
		"pageLength": 5,
		"ordering": false,
		"searching": true,
		"lengthChange": false,
		/*"columnDefs": [{
			"targets": -1,
			"data": null,
			"defaultContent": '<button type="button" class="btn btn-primary btn-sm">DO</button>'
		}]*/
		"columnDefs":
		[
			// The 'data' parameter refers to the data for the cell (defined by the
			// 'data' option, which defaults to the column being worked with, in
			// this case 'data: 0'.
			{
				"render": function (data, type, row) {
					//return data + ' (' + row[3] + ')';
					return '<img src="'+data+'" style="height:50px;"/>'
				},
				"targets": 0
			},
			{
				"render": function (data, type, row) {
					//return data + ' (' + row[3] + ')';
					if(_MORE4CREATE.IsOnlyCopy[_aIndex] == true)
					{
						return '<table><tr><td><button type="button" class="btn btn-primary btn-sm" onclick="copyToClipboard('+"'" + row[2] + "'" +')">Copy</button></td></tr></table>'
					}
					else
					{
						return '<table><tr><td><button type="button" class="btn btn-primary btn-sm" onclick="copyToClipboard('+"'" + row[2] + "'" +')">Copy</button></td><td><button type="button" class="btn btn-primary btn-sm" onclick="preparePlace('+"'"+row[2]+"'," + data+','+"'"+_entity+"'"+',false)">Embed</button></td></tr></table>'
					}
				},
				"targets": 4
			},
			{
				"visible": !_MORE4CREATE.IsOnlyCopy[_aIndex],
				"targets": 0
			},
			{
				"visible": !_MORE4CREATE.IsOnlyCopy[_aIndex],
				"targets": 1
			}
		]
	});
	$('.dataTables_length').addClass('bs-select');
	
	//REQUEST DATA
	prepareEntity(_entity);
}

function prepareEntity(_entity)
{
	APICALL('json','search/pick/'+_entity,'POST');
}

function prepareDownload(_imageID,_entity,_code)
{
	
	alertify.success('Preparing download...');
	APICALL('file','file/download/DAGetFileInfo/fileid/'+_imageID+'/filename/newfilename','GET',_entity,_code);
}

function copyToClipboard(code,custom)
{
	try
	{
		var node = document.createElement( "textarea" );
		node.innerHTML = code;
		document.body.appendChild( node );
		node.select();
		document.execCommand("copy");
		document.body.removeChild( node );
		if(custom)
		{
			alertify.success("Copied: " + code)
		}
		else
		{
			alertify.success("Copied to clipboard")
		}
	}
	catch(err)
	{
		alertify.error("Error");
	}
}

function preparePlace(_codeForName,_imageID,_entity,_isAfterDownload)
{
	if(parseInt(_imageID)>0)
	{
		CSI.evalScript("placeItem('"+_LOCALGALLERY + _entity + '/'+"',"+_imageID+","+_isAfterDownload+",'"+_codeForName+"');", preparePlaceAfter);
	}
	else{ alertify.warning('No image associated'); }
}

function preparePlaceAfter(event)
{
	if(event.indexOf('not found') > -1 || event.toUpperCase().indexOf('ERROR ') > -1)
	{
		alertify.error(event);
	}
	else
	{
		if(event == 'Embed')
		{
			alertify.success(event);
		}
		else
		{
			prepareDownload(event.split('||')[0],
							event.split('||')[1].replace(_LOCALGALLERY,'').replace('/',''),
							event.split('||')[2]);
		}
	}
}

function doParam(_controlID)
{
	var _controlValue = $("#f"+_controlID).val();
	_controlValue = _controlValue.replace(/\"/g,'\\"');
	return _controlValue;
}

function saveRecord()
{
	var _ENTITY = $("select.custom-select").val();
	if(_ENTITY != null)
	{
		alertify.success("Saving data, please wait...",600)
		if (window.XMLHttpRequest)
			var http_request = new XMLHttpRequest();
		else
			if (window.ActiveXObject)
				var http_request = new ActiveXObject("Microsoft.XMLHTTP");

		if (http_request) {
			var __dataFields = ''
			var _aIndex = _MORE4CREATE.Entity.indexOf(_ENTITY);
			var fieldsEntity = _MORE4CREATE.Fields[_aIndex].split('|');
			for (let iField = 0; iField < fieldsEntity.length; iField++)
			{
				__dataFields += '"'+fieldsEntity[iField]+'":"' + doParam(fieldsEntity[iField]) +'",';
			}
			__dataFields = __dataFields.substr(0,__dataFields.length-1);

			if(_DOCUMENT_IMG_ID_FROM_MoDA > -1 && $('#id_digitalasset').prop('checked') && _MORE4CREATE.IMG[_aIndex] != "n/a")
			{
				__dataFields += ',"' + _MORE4CREATE.IMG[_aIndex] + '":' + _DOCUMENT_IMG_ID_FROM_MoDA;
			}

			var __dataParams = '{"Entity": { ' + __dataFields + ' }}';
			http_request.onreadystatechange = function () { eval("saveRecordAfter(http_request)"); };
			http_request.open("PUT", _APIURL + "bo/"+_ENTITY+"/");
			http_request.setRequestHeader("Authorization","Bearer "+_APIKEY);
			http_request.setRequestHeader("Content-Type","application/json");
			http_request.responseType = "json";
			http_request.send(__dataParams);
		}
	}
	else{ alertify.error("Please select a type") }
}

function saveRecordAfter(http_request)
{
	if(http_request.readyState==4)
	{
		alertify.dismissAll();
		if(http_request.status==200)
		{
			var IsSuccess = http_request.response.IsSuccess;
			if(IsSuccess){ alertify.success("Record successfully created"); }
			else { alertify.error(http_request.response.Result); }
		}
		else
		{
			APICALLOnError(http_request);
		}
	}
}
/*
t.cell({row:2, column:0}).data('file://C:/Users/amartel/Documents/NGC/Library/color/8119.png');
var names = t.rows( function (idx, data, node) {
					return data[1] == 208 ? t.cell(idx,2).data('XX WAIT') : false;
				});
*/