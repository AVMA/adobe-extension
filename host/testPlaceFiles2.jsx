﻿doc = app.activeDocument;

pathx = "C:\\Users\\amartel\\Pictures\\HD\\"
pathx = "C:/Users/amartel/Documents/NGC/Library/labelpackcode/";

var dirImages = new Folder(pathx);
var imagesList = dirImages.getFiles();

//var itemToPlace = {};

for (var i = 0; i < imagesList.length; i++) {

	var imgName = imagesList[i].name;
	var documentName = doc.name;

	var doc = app.activeDocument;
	var itemToPlace = doc.placedItems.add();


	//var x = getXMPValue(imagesList[i], "PixelXDimension");  
	//var y = getXMPValue(imagesList[i], "PixelYDimension");  

	itemToPlace.file = imagesList[i];
	itemToPlace.layer = doc.currentLayer;
	itemToPlace.top = doc.height;
	itemToPlace.left = 0;
 }

function getXMPValue(obj, tag) {  
  var xmp = "";  
  if (obj == undefined) {  
    Error.runtimeError(2, "obj");  
  }  
  if (tag == undefined) {  
    Error.runtimeError(2, "tag");  
  }  
  if (obj.constructor == String) {  
    xmp = new XML(obj);  
  } else if (obj.typename == "Document") {  
    xmp = new XML(obj.xmpMetadata.rawData);  
  } else if (obj instanceof XML) {  
    xmp = obj;  
  } else if (obj instanceof File) {  
    if (!ExternalObject.AdobeXMPScript) {  
      ExternalObject.AdobeXMPScript = new ExternalObject('lib:AdobeXMPScript');  
    }  
    // Stdlib.loadXMPScript();  
    if (tag == "CreateDate") {  
      var cstr = obj.created.toISODateString();  
      var mstr = Stdlib.getXMPValue(obj, "ModifyDate");  
      return cstr += mstr.slice(mstr.length-6);  
    }  
    // add other exceptions here as needed  
    var fstr = decodeURI(obj.fsName);  
    var xmpFile = undefined;  
    try {  
      xmpFile = new XMPFile(fstr, XMPConst.UNKNOWN,  
                            XMPConst.OPEN_FOR_READ);  
    } catch (e) {  
      try {  
        xmpFile = new XMPFile(fstr, XMPConst.UNKNOWN,  
                              XMPConst.OPEN_USE_PACKET_SCANNING);  
      } catch (e) {  
        Error.runtimeError(19, "obj");  
      }  
    }  
    var xmpMeta = xmpFile.getXMP();  
    var str = xmpMeta.serialize()  
    xmp = new XML(str);  
    xmpFile.closeFile();  
  } else {  
    Error.runtimeError(19, "obj");  
  }  
var s;  
   
 // Handle special cases here  
 if (tag == "ISOSpeedRatings") {  
  s = String(eval("xmp.*::RDF.*::Description.*::ISOSpeedRatings.*::Seq.*::li"));  
  } else {  
    // Handle typical non-complex fields  
  s = String(eval("xmp.*::RDF.*::Description.*::" + tag));  
  }  
  return s;  
};