function getAndromedaInfo(userDataFolder)
{
	var fContent = "";
	var previewInfoFile = File(userDataFolder+"/DataStorage/LocalStorage.json");
	if(previewInfoFile.exists)
	{
		previewInfoFile.encoding = "UTF-8";
		if(previewInfoFile.open())
		{
			fContent = previewInfoFile.read();
			previewInfoFile.close();
		}
	}
	return fContent;
}

function getDAMDesktop_FileSync(userDataFolder)
{
	var fContent = "";
	var previewInfoFile = File(userDataFolder+"/DataStorage/MinSQL/DAMDesktop_FileSync.json");
	if(previewInfoFile.exists)
	{
		previewInfoFile.encoding = "UTF-8";
		if(previewInfoFile.open())
		{
			fContent = previewInfoFile.read();
			previewInfoFile.close();
		}
	}
	return fContent;
}

function placeItem(folder,DA_ID,isAfterDownload,code)
{
	var isPlaced = DA_ID + "||" + folder + "||" + code;
	var dirImages 	= new Folder(folder);
	var imagesList 	= dirImages.getFiles();
	var currImage	= '';
	for (var iFile = 0; iFile < imagesList.length; iFile++) {
		currImage = imagesList[iFile].name;
		if(currImage.indexOf(DA_ID+'_')>-1)
		{
			var doc = app.activeDocument;
			var itemToPlace = doc.placedItems.add();
			itemToPlace.file = imagesList[iFile];
			// Avoid resize unless needed and size relation is kept
			//itemToPlace.width = 100;
			//itemToPlace.height = 100;
			itemToPlace.layer = doc.currentLayer;
			//itemToPlace.top = doc.height;
			//itemToPlace.left = 0;
			itemToPlace.position = Array(0,0);
			itemToPlace.name = Date.now() + "~^*" + code;
			itemToPlace.embed();
			imagesList[iFile].remove();
			isPlaced = "Embed";
			break;
		}
	}
	if(isAfterDownload && isPlaced != "Embed")
	{
		isPlaced = "Image not found in local library";
	}
	return isPlaced;
}

function getPlacedItemsList_NO()
{
	var placedItem = "";
	var placedItems = new Array();
	var doc = app.activeDocument;
	for(var pItem = 0; pItem < doc.placedItems.length; pItem++)
	{
		placedItem = doc.placedItems[pItem].name;
		if(placedItem != "")
		{
			placedItems.push(placedItem);
		}
	}
	return placedItems.join(",");
}

function getPlacedItemsList()
{
	if ( app.documents.length > 0 )
	{
		var fileReferences = new Array();
		var sourceDoc = app.activeDocument;
		for (i = 0; i < sourceDoc.pageItems.length; i++)
		{
			artItem = sourceDoc.pageItems[i];
			switch (artItem.typename.toLowerCase()) {
				case "groupitem":
					if (artItem.name != "")
					{
						fileReferences.push(artItem.name);
					}
					break;
				case "rasteritem":
					if (artItem.embedded && artItem.name != "")
					{
						fileReferences.push(artItem.name);
					}
					break;
			}
		}

		var filesInOrderEmbeed = fileReferences.sort();
		for(var metaFile in filesInOrderEmbeed)
		{
			filesInOrderEmbeed[metaFile] = filesInOrderEmbeed[metaFile].substr(filesInOrderEmbeed[metaFile].lastIndexOf('~^*')+3)
		}

		return filesInOrderEmbeed.join(",");
	}
}

function JSXgetDocumentName()
{
	return app.activeDocument.name;
}

function JSXAlert(pMessage)
{
	alert(pMessage);
}